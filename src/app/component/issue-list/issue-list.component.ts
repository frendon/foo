import {Component, OnInit} from '@angular/core';
import {IssueService} from 'src/app/service/issue.service';
import {Issue} from 'src/app/model/issue';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';


@Component({
  selector: 'app-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrls: ['./issue-list.component.css']
})
export class IssueListComponent implements OnInit {

  issues: any = [];
  author: any = [];
  private issue: any;

  constructor(public serviceIssues: IssueService) {
    serviceIssues.getIssues().subscribe((data: {}) => {
      console.log(data);
      this.issues = data;
      
    });

  }

  mostrarIssues() {
    this.serviceIssues.getIssues();
  }

  ngOnInit() {

  }

  onSelect(issue) {
    this.issue = issue;
  }
}
