import {Component, OnInit} from '@angular/core';

import {IssueService} from 'src/app/service/issue.service';
import {ActivatedRoute} from '@angular/router';

import {Location} from '@angular/common';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.css']
})
export class IssueDetailComponent implements OnInit {

  issue: any = [];


  constructor(public serviceIssues: IssueService, router: ActivatedRoute, private location: Location) {

    const id = router.snapshot.paramMap.get('iid');
    console.log(id);
    serviceIssues.getIssue(id).subscribe((data: {}) => {
      console.log(data);
      this.issue = data[0];

    });
  }

  ngOnInit() {

  }
  
  savedIssue() {
    this.serviceIssues.addIssue(this.issue);
    this.location.back();
  }


}
