import { Author } from './author';

export class Issue {

public  id: number;
public  iid: number;
public  project_id: number;
public  title: string;
public  descripcion: string;
public  state: string;
public  created_at: Date;
public  update_at: Date;
public  closet_at: Date;
public  labels: Array<string>;
public  milestone: string;
public  assigness: Array<string>;
public  author: Author;
public  assignee: any;
public  user_notes_count: number;
public  upvotes: number;
public  downvotes: number;
public  due_date: Date;
public  confidential: boolean;
public  discussion_locked: any;
public  web_url: string;
public  weight: number;
}
