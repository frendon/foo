export class Author {
  constructor(
    public id: number,
    public name: string,
    public username: string,
    public state: string,
    public avatar_url: string,
    public web_url: string
  ) { }
}
