import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssueListComponent } from './component/issue-list/issue-list.component';
import { IssueDetailComponent } from './component/issue-detail/issue-detail.component';

const routes: Routes = [
  { path: '', component: IssueListComponent },
  { path: 'issue-list', component: IssueListComponent },
  { path: 'issue-add/:iid', component: IssueDetailComponent },
  { path: 'issue-add', component: IssueDetailComponent }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
