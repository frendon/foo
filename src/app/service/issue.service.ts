import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Issue } from '../model/issue';
import { BrowserDomAdapter } from '@angular/platform-browser/src/browser/browser_adapter';

@Injectable({
  providedIn: 'root'
})


export class IssueService {
  
  issues:any = [];

  constructor(private http: HttpClient) {
  }

  endpoint = "https://gitlab.com/api/v4/";
  tokken = "DkNG9a4oKkJMbntSAxAX";
  projectId = 9958762;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'PRIVATE-TOKEN': 'DkNG9a4oKkJMbntSAxAX'
    })
  };

 
  getIssues(): Observable<any> {
    return this.http.get(this.endpoint + "issues?private_token=" + this.tokken).pipe(
      map(this.extractData));
  }

  getIssue(iid): Observable<any> {
    return this.http.get(this.endpoint + "issues?private_token=" + this.tokken + "&iids[]=" + iid).pipe(
      map(this.extractData));
  }

  addIssue (issue): Observable<any> {
    console.log(issue);
    issue.title = 'title test' + issue.id;
    issue.id = 10;
    
    console.log(JSON.stringify(issue));

    return this.http.post<any>(this.endpoint + "projects/" + this.projectId + "/issues/?", issue, this.httpOptions).pipe(
      tap((product) => console.log(`added issue w/ id=${issue.id}`)),
      catchError(this.handleError<any>('addIssue'))
    );
  }

  updateIssue (iid, issue): Observable<any> {
    return this.http.put(this.endpoint + 'projects/' + this.projectId + "/issues/" + iid + "?", JSON.stringify(issue), this.httpOptions).pipe(
      tap(_ => console.log(`updated issue id=${iid}`)),
      catchError(this.handleError<any>('updateIssue'))
    );
  }


  deleteIssue (iid): Observable<any> {
    return this.http.delete<any>(this.endpoint + 'projects/' + this.projectId + "/issues/" + iid, this.httpOptions).pipe(
      tap(_ => console.log(`deleted issue id=${iid}`)),
      catchError(this.handleError<any>('deleteIssue'))
    );
  }

  //Board  
  getBoard (): Observable<any> {
    let endpointboard = "https://gitlab.com/api/v4/projects/" + this.projectId + "/boards";
    return this.http.get(endpointboard, this.httpOptions).pipe(
      map(this.extractData));    
  }



  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
